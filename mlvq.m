function [ codebook, codelabels ] = mlvq(datacell, labels, numclass, numtrain, codenum, alpha, epoch)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % datacell:     contains data matrix of observations (rows); it is  %
    %               assumed each data matrix has m rows, and each row   %
    %               of each data matrix are representative of the       %
    %               same sample.                                        %
    % labels:       labels corresponding to each observation.           %
    % numclass:     number of total classes.                            %
    % numtrain:     number of training vectors per class                %
    % codenum:      number of codebook vectors per class.               %
    % alpha:        learning rate.                                      %
    % epoch:        number of epochs to run the training for.           %
    %                                                                   %
    % other                                                             %
    % requirements: data must be grouped in blocks of numtrain vectors  %
    %               i.e. [car; car; car; train; train; train]           %
    %                                                                   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % initialization                                                    %               
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % init codebooks
    [m,n] = size(datacell{1});
    [v,z] = size(datacell);
    
    codebook = cell(v,1);
    
    for i=1:v
        tic
        fprintf('\nInit codelabels%i ',i)
        [codebook{i}, codelabels] = generatecb(datacell{i},codenum,numclass,numtrain,1000,50,labels);
        fprintf('\ndone: ')
        toc
    end
    
    % set alpha value
    [mc, nc] = size(codebook{1});
    alphav = zeros(mc,1);
    alphav(:,1) = alpha;
    
    findcbi = zeros(v,1);
    
    % run mlvq for a number of epochs
    for i=1:epoch
        
        if mod(i,1000) == 1
            tic
            fprintf('\nepoch %i ',i)
        end
        
        [q,z] = size(datacell);
        maxdim = zeros(q,1);
        for b=1:q
           [bq,zq] = size(datacell{b});
           maxdim(b) = zq;
        end
        
        maxdim = max(maxdim);
        
        x = zeros(v,maxdim);
        
        % select a random row from each view and find the winning codebook
        % vector
        xind = randi(m,1);
        for j=1:v
            [m,n] = size(datacell{j});
            x(j,1:n) = datacell{j}(xind,1:n);
            findcbi(j) = findcb(codebook{j}, x(j,1:n));
        end
        
        % learning adaption for each view
        for j=1:v
            
            wcodeind = findcbi(j);

            if ( codelabels(wcodeind) == labels(xind) )

                for c=1:v
                    
                    [m,n] = size(datacell{c});
                    codebook{c}(wcodeind,1:n) = codebook{c}(wcodeind,1:n) ...
                        + alphav(wcodeind).*(x(c,1:n)-codebook{c}(wcodeind,1:n));
                end

                % update alpha
                alphav(wcodeind) = alphav(wcodeind) / (1 + alphav(wcodeind));

            else

                for c=1:v
                    
                    [m,n] = size(datacell{c});
                    codebook{c}(wcodeind,1:n) = codebook{c}(wcodeind,1:n) ...
                        - alphav(wcodeind).*(x(c,1:n)-codebook{c}(wcodeind,1:n));
                end

                % update alpha
                tempalpha = alphav(wcodeind) / (1 - alphav(wcodeind));
                if tempalpha < alphav(wcodeind)
                    alphav(wcodeind) = tempalpha;
                end

            end
        end
        
        
        if mod(i,1000) == 0
            fprintf('to epoch %i of %i done: ',i,epoch)
            toc
        end
        
    end    
end

