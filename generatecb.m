function [ codebook, codelabels, ind ] = generatecb(data,codenum,numclass,numsamples,iter,replicate,labels)
    
    warning('off','all');
    
    codelabels = zeros(codenum*numclass,1);
    codebook = [];
    opts = statset('MaxIter',iter);
    
    ind = [];

    for i=1:numclass
        
        [idx,C] = kmeans(data((i-1)*numsamples+1:i*numsamples,:),codenum,'options',opts,'replicates',replicate);
        codebook = [codebook;C];
        
        for j=(i-1)*codenum+1:i*codenum
            codelabels(j) = labels((i-1)*numsamples+1);
        end
        
        ind = [ind idx];
    end
end
