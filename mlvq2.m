function [ codebook, codelabels ] = mlvq2(datacell, codebook, codelabels, labels, numclass, numtrain, codenum, alpha, epoch, window)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % datacell:     contains data matrix of observations (rows); it is  %
    %               assumed each data matrix has m rows, and each row   %
    %               of each data matrix are representative of the       %
    %               same sample.                                        %
    % labels:       labels corresponding to each observation.           %
    % numclass:     number of total classes.                            %
    % numtrain:     number of training vectors per class                %
    % codenum:      number of codebook vectors per class.               %
    % alpha:        learning rate.                                      %
    % epoch:        number of epochs to run the training for.           %
    % window:       size of window to consider local movements.         %
    %                                                                   %
    % other                                                             %
    % requirements: data must be grouped in blocks of numtrain vectors  %
    %                                                                   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % initialization                                                    %               
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % init codebooks
    [m,n] = size(datacell{1});
    [v,z] = size(datacell);
    
    %for i=1:v
    %    tic
    %    fprintf('\nInit codelabels%i ',i)
    %    [codebook{i}, codelabels] = generatecb(datacell{i},codenum,numclass,numtrain,1000,50,labels);
    %    fprintf('\ndone: ')
    %    toc
    %end
    
    % set alpha value
    [mc, nc] = size(codebook{1});
    
    findcbi = zeros(v,2);
    dist = zeros(v,2);
    
    % run mlvq for a number of epochs
    for i=1:epoch
        
        if mod(i,1000) == 1
            tic
            fprintf('\nepoch %i ',i)
        end
        
        [q,z] = size(datacell);
        maxdim = zeros(q,1);
        for b=1:q
           [bq,zq] = size(datacell{b});
           maxdim(b) = zq;
        end
        
        maxdim = max(maxdim);
        
        x = zeros(v,maxdim);
        
        s = (1-window)/(1+window);
        
        % select a random row from each view and find the winning codebook
        % vector
        xind = randi(m,1);
        for j=1:v
            [m,n] = size(datacell{j});
            x(j,1:n) = datacell{j}(xind,1:n);
            [findcbi(j,:),dist(j,:)] = knnsearch(codebook{j},x(j,1:n),'k',2);
        end

        % learning adaption for each view
        for j=1:v
            
            wcodeind = findcbi(j,1);
            wcodeind2 = findcbi(j,2);
            
            d1 = dist(j,1)/dist(j,2);
            d2 = dist(j,2)/dist(j,1);
            
            cor1 = (codelabels(wcodeind) == labels(xind));
            cor2 = (codelabels(wcodeind2) == labels(xind));
            cordif = (cor1 ~= cor2);
            
            if (cor1 == 1)
                indwin = wcodeind;
                indrun = wcodeind2;
            else
                indwin = wcodeind2;
                indrun = wcodeind;
            end

            if ( ( min(d1,d2) > s ) && ( cordif == 1 ) )

                for c=1:v
                    
                    [m,n] = size(datacell{c});
                    codebook{c}(indwin,1:n) = codebook{c}(indwin,1:n) ...
                        + alpha.*(x(c,1:n)-codebook{c}(indwin,1:n));
                end

                % update alpha
                alpha = alpha / (1 + alpha);

                for c=1:v
                    
                    [m,n] = size(datacell{c});
                    codebook{c}(indrun,1:n) = codebook{c}(indrun,1:n) ...
                        - alpha.*(x(c,1:n)-codebook{c}(indrun,1:n));
                end

                % update alpha
                tempalpha = alpha / (1 - alpha);
                if tempalpha < alpha
                    alpha = tempalpha;
                end

            end
        end
        
        
        if mod(i,1000) == 0
            fprintf('to epoch %i of %i done: ',i,epoch)
            toc
        end
        
    end    
end

